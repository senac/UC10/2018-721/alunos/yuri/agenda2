/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agenda_OO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Agenda {

    private String nome;
    private String cor;
    private List<Pessoa> contatos;

    public Agenda() {
        this.contatos = new ArrayList<>();
    }

    public Agenda(String nome) {
        this();
        this.nome = nome;
    }

    public Agenda(String nome, String cor) {
        this(nome);
        this.cor = cor;

    }

    public void adicionarContato(Pessoa p) {

        if (!this.isContatoNaAgenda(p)) {
            this.contatos.add(p);
        } else {
            throw new RuntimeException("Contato ja existente !");
        }

    }

    public void removerContato(Pessoa p) {
        if (this.isContatoNaAgenda(p)) {
            this.contatos.remove(p);
        } else {
            throw new RuntimeException("Contato não esta na agenda !");
        }
    }

    public void removerContato(String nome) {

        for(int i = 0 ; i < this.contatos.size() ; i++ ){
            if(this.contatos.get(i).getNome().equalsIgnoreCase(nome)){
                this.contatos.remove(i);
            }
        }
    }
    
    public int getQuantidadeContatos(){
        return  this.contatos.size() ; 
    }
    
    public Pessoa BuscarContato(String nome){
        for(int i = 0 ; i < this.getQuantidadeContatos() ; i ++){
            if(this.contatos.get(i).getNome().equals(nome)){
                return this.contatos.get(i) ; 
            }
        throw new RuntimeException("Contato nao existe na lista!!");
        }
        
        return null ; 
    }
    
    public boolean isContatoNaAgenda(Pessoa p ){
        
        return  this.contatos.contains(p) ; 
    }
    
    public String Listar(){
        if(this.getQuantidadeContatos() == 0){
            throw new RuntimeException("Lista Vazia!!");
        }else{
            String lista = "";
            for (int i = 0; i < this.getQuantidadeContatos(); i++) {
                lista += "Lista de Contatos["+ (i+1) + "º" + "] - " + " Nome: " 
                        + this.contatos.get(i).getNome() + " - " + " Telefone: " 
                        + this.contatos.get(i).getTelefone() + "\n";
            }
            return lista;
        }
    }

   public void ExportarDados(){
       File file = new File("Agenda.txt");
                try{
                    FileWriter fw;
                    if(!file.exists()){
                        file.createNewFile();
                        fw = new FileWriter(file);
                    }else{
                        fw = new FileWriter(file, true);
                    }
                    
           for (Pessoa x : this.contatos  ) {
               
               fw.write(x.toString() +"\n");
           }   
                    fw.close();
                    
                }catch (IOException ex){
                    ex.printStackTrace();
                }
   }
    
   public String ListaVazia(){
       if(this.contatos.isEmpty()){
           
           throw new RuntimeException("Lista vazia");
       }
   return null;
   }
    
   
   public void ImportarDados() throws IOException {
       String arq;
       Scanner scn = new Scanner(System.in);
       FileInputStream stream = null;
        try {
            System.out.print("Digite o nome do 'arquivo'.txt");
            arq = scn.next();
            stream = new FileInputStream(arq);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
    InputStreamReader reader = new InputStreamReader(stream);
    BufferedReader br = new BufferedReader(reader);
    String linha = br.readLine();
    while(linha != null) {
   }

   }

public boolean ExisteNome(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean ExisteTelefone(String telefone) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getTelefone().equals(telefone)) {
                return true;
            }
        }
        return false;
    }
    
 public String VerificarTelefone(String telefone) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getTelefone().equals(telefone)) {
                return "O telefone já é cadastrado deseja continuar assim mesmo?";
            }
        }
        return null;
    }
 
     public String VerificarNome(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return "O nome já é cadastrado deseja continuar assim mesmo?";
            }
        }
        return null;
    }



}