
package Agenda_OO;


public class Pessoa {
    
    
    String nome;
    String telefone;
    
    public Pessoa(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }
    
    public String getNome(){
        return nome;
    }
    
    public void setNome(String nome){
       this.nome = nome;
    }
    
    public String getTelefone(){
        return telefone;
    }
    
    public void setTelefone(String telefone){
        this.telefone = telefone;
    }
    
    
    @Override
    public String toString(){
        return "Nome:" + this.nome + " - "
                + "Telefone:" + this.telefone + "  ";
    }
    
    
    public boolean equals(Object object){
        if(object instanceof Pessoa){
            Pessoa pessoa = (Pessoa) object;
            return ((this.nome.equals(pessoa.getNome())) &&(this.telefone.equals(pessoa.getTelefone())));
        }
        return false;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
