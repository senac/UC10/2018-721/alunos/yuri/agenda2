/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agenda_OO;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author sala304b
 */
public class App {
    public static void main(String[] args) throws IOException {
        Agenda agenda = new Agenda();
        
        int opçao;
        
        Pessoa p = null;
        try {
            Scanner scanner = new Scanner(new FileReader("contatos.txt"))
                    .useDelimiter("@#@");
            while (scanner.hasNext()) {
                String nome = scanner.next();
                String telefone = scanner.next();

                p = new Pessoa(nome, telefone);

                agenda.adicionarContato(p);

            }

        } catch (IOException ex) {

            ex.printStackTrace();

        }
        
        do{
           
        String aux = JOptionPane.showInputDialog("__________________Agenda__________________\n"
                        + "Contatos("+ agenda.getQuantidadeContatos() +")"
                        
                + "\n" + "(1)NOVO"
                + "\n" + "(2)DELETAR"
                + "\n" + "(3)PESQUISAR"
                + "\n" + "(4)LISTAR TODOS"
                + "\n" + "(5)GRAVAR AGENDA EM DISCO"
                + "\n" + "(6)SAIR"
                + "\n" + "-----------------------------------------------------"
                + "\n" + "Digite uma opção:");
        opçao = Integer.parseInt(aux);
        
        
        
        switch(opçao){
            case 1:
        
                String nome = JOptionPane.showInputDialog("Nome:");
                if (agenda.ExisteNome(nome)) {
                            nome = JOptionPane.showInputDialog(agenda.VerificarNome(nome) + "\n"
                                    + "Sim/Não?");
                            if (nome.equalsIgnoreCase("N")) {
                                break;
                            }
                        }
                String telefone = JOptionPane.showInputDialog("Telefone:");
                if (agenda.ExisteTelefone(telefone)) {
                            telefone = JOptionPane.showInputDialog(agenda.VerificarTelefone(telefone) + "\n"
                                    + "Sim/Não?");
                            if (telefone.equalsIgnoreCase("N")) {
                                break;
                            }
                        }
                p = new Pessoa(nome, telefone);
                agenda.adicionarContato(p);

                    JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
                    
                break;
            case 2:
                
                nome = JOptionPane.showInputDialog("Contato a ser deletado:");
                agenda.removerContato(nome);
                JOptionPane.showMessageDialog(null, "Contato deletado!!");
               
                break;
                
            case 3:
                agenda.ListaVazia();
                nome = JOptionPane.showInputDialog("Contato a ser buscado:");
                agenda.BuscarContato(nome);
                JOptionPane.showMessageDialog(null, "Contato existe na lista!!"
                        + p.toString() );
                     
                break;
            case 4:
                JOptionPane.showMessageDialog(null,agenda.Listar());
                break;
            case 5:
                
                agenda.ExportarDados();
                JOptionPane.showMessageDialog(null, "Salvos com sucesso!");
                
                break;
            case 6:    
                System.exit(0);
        
            default:
                JOptionPane.showMessageDialog(null, "Opção Invalida!!!");
        
        }
          
    }while(true);
    
}
}